/*
 * Materialien:
 * 16in1_DE/MQ-2 Gas Sensor Module DE.pdf
 * (ESP-32 Dev Kit C V2_DE.pdf)
 * 
 * Um den Output zu sehen
 * Tools >> Serial Monitor
 */

#define READ_PIN 0 //GPIO0

float sensorValue;
int gas_threshold = 100; //depends on the sensor

void setup() {
  Serial.begin(9600);
  Serial.println("The gas sensor is warming up...");
  delay(30000);
}

void loop() {
  sensorValue = analogRead(READ_PIN);
  Serial.print("Sensor Value: ");
  Serial.print(sensorValue);

  if(sensorValue > gas_threshold) {
    Serial.print("Gas detected!");
  }
  
  Serial.println("");
  delay(2000); // wait 2s for next reading
}
