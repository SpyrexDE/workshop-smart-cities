/*
 * Materialien:
 * 16in1_DE/KY-033 Line-follower Sensor Module DE.pdf
 * (ESP-32 Dev Kit C V2_DE.pdf)
 * 
 * Um den Output zu sehen
 * Tools >> Serial Monitor
 */


#define DIGITAL_PIN 2

boolean line = false;

void setup() {
  Serial.begin(9600);
  pinMode(DIGITAL_PIN, INPUT);
}

void loop() {
  line = digitalRead(DIGITAL_PIN);
  
  if (line) {
    Serial.println("Line detected!");
  }

  delay(1000);
}
