/*
 * Materialien:
 * 16in1_DE/Neihunhssensor_DE.pdf
 * (ESP-32 Dev Kit C V2_DE.pdf)
 * 
 * Um den Output zu sehen
 * Tools >> Serial Monitor
 */

#define DIGITAL_PIN 2

boolean tilting = false;
String tilt;

void setup() {
  Serial.begin(9600);
  pinMode(DIGITAL_PIN, INPUT);
}

void loop() {
  tilting = digitalRead(DIGITAL_PIN);
  if (tilting) {
    tilt = "No";
  }
  else {
    tilt = "Yes";
  }

  Serial.print("Tilt detected: ");
  Serial.println(tilt);
  delay(2000);
}
