import logging

from flask import Flask, render_template, request, jsonify
from flask_socketio import SocketIO
import paho.mqtt.client as mqtt

import config
from app.models.Logger import mqtt_logger, logger
from config import *

def topicForBuilding(topicStr):
    topicStr = str(topicStr).lower().strip().split("/")[0]
    buildings = ["feuerwache",
        "hochhaus",
        "wohnhaus",
        "tiefgarage",
        "einkaufszentrum",
        "katastrophenschutz",
        "wetterstation",
        "solarpark",
        "flughafen",
        "schule",
        "polizeistation"]
    return topicStr in buildings

def on_connect(client, userdata, flags, rc):
    if rc == 0:
        logger.info("Connected to MQTT Broker!")
    else:
        logger.warning("Failed to connect, return code " + str(rc) + "\n")

def on_message(client, userdata, msg):
    mqtt_logger.info(f"Received `{msg.payload.decode()}` from topic `{msg.topic}`")
    try:
        if msg.topic == "$SYS/broker/clients/total":
            socketio.emit("clients", {'data': msg.payload.decode(), 'topic': msg.topic})
        elif msg.topic == "zeit":
            socketio.emit("zeit", {'data': msg.payload.decode(), 'topic': msg.topic})
        elif topicForBuilding(str(msg.topic)):
            socketio.emit("mqtt_messages", {'data': msg.payload.decode(), 'topic': msg.topic})
        socketio.emit("all_messages", {'data': msg.payload.decode(), 'topic': msg.topic})
    except Exception:
        logger.warn("MQTT Message not able to send via Websockets.")

try:
    # try connecting with MQTT broker
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message
    client.username_pw_set(username=config.user, password=config.pwd)
    client.connect(host, port, keep_alive)
    client.subscribe("+")
    client.subscribe("+/+")
    client.subscribe("$SYS/broker/clients/total")
    client.loop_start()
except Exception as e:
    logger.error(str(e))
    raise SystemExit

# create app
app = Flask(__name__)
app.config['SECRET_KEY'] = secret_key

# create socketio object
socketio = SocketIO(app)

@socketio.on('connect')
def handle_connect():
    logger.info('Client connected, IP: ' + str(request.remote_addr) + ', User Agent: ' + str(request.headers.get('User-Agent')))

# Add logging
logging.basicConfig(filename='logging/flask.log', filemode='w', level=logging.DEBUG, format=f'%(asctime)s %(levelname)s %(name)s %(threadName)s : %(message)s')

@app.route('/')
def index():
    return render_template('index.html')

@app.route('/dashboard')
def dashboard():
    return render_template('dashboard.html')

@app.route('/mqtt-console')
def mqttConsole():
    return render_template('mqtt-console.html')