function onclick_stopBtn() {
    // set global stop variable
    stopBtnActive = true;

    // hide Stop Btn
    document.getElementById("stopBtn").classList.add("disabled");

    // show Resume Btn
    document.getElementById("resumeBtn").classList.remove("disabled");

    console.log("Stopping MQTT console successful.")
}

function onclick_resumeBtn() {
    // set global stop variable
    stopBtnActive = false;

    // hide Resume Btn
    document.getElementById("resumeBtn").classList.add("disabled");

    // show Stop Btn
    document.getElementById("stopBtn").classList.remove("disabled");

    console.log("Resuming MQTT console successful.")
}