function isNullOrEmpty(value) {
  return value === null || String(value).trim() === "" || String(value) === "undefined";
}