// Globale Variable für das Chart-Objekt deklarieren
var chart1;

// Chart erstellen
function createClientsCharts() {
  chart1 = new Chart(document.getElementById('myChart1'), {
    type: 'line',
    data: {
      labels: [],
      datasets: [{
        label: 'Clients',
        data: [],
        fill: false,
        borderColor: 'rgb(75, 192, 192)',
        tension: 0.1
      }]
    },
    options: {
      responsive: true,
      plugins: {
        title: {
          display: true,
          text: 'Echtzeitdaten'
        }
      },
      scales: {
        x: {
          title: {
            display: true,
            text: 'Zeit'
          }
        },
        y: {
          title: {
            display: true,
            text: 'Wert'
          }
        }
      }
    }
  });
}

// Chart aktualisieren
function updateClientsChart(date, value) {
  // TODO: Fehlerbehandlung
  var data = {
    x: date,
    y: value
  };
  if (!chart1) {
    console.log("Chart ist noch nicht initialisiert");
    return;
  }
  // Datenpunkt zur Datenreihe hinzufügen
  chart1.data.labels.push(data.x);
  chart1.data.datasets[0].data.push(data.y);
  // Chart neu zeichnen
  chart1.update();
}