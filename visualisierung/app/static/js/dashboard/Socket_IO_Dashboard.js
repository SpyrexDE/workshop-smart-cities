var socket = io.connect('http://' + document.domain + ':' + location.port);

socket.on("connect", function () {
  hideNotConnectedBanner();
  console.log("Verbindung zum WebSocket-Server hergestellt.");
  connected();
});

socket.on("disconnect", function () {
  console.log("Verbindung zum WebSocket-Server unterbrochen.");
  showNotConnectedBanner();
  disconnected();
});

socket.on("reconnect", function (attemptNumber) {
  console.log("Verbindung zum WebSocket-Server wiederhergestellt. Versuch Nr.:", attemptNumber);
  alert("Die Verbindung zum Server wurde wiederhergestellt.");
  hideNotConnectedBanner();
  connected();
});

socket.on("reconnect_failed", function () {
  showNotConnectedBanner();
  console.log("Alle Wiederherstellungsversuche sind fehlgeschlagen.");
  disconnected();
});

// Event-Listener für eingehende WebSockets-Meldungen hinzufügen
socket.addEventListener('clients', function (event) {
  const data = parseInt(JSON.parse(event.data));
  const date = new Date(); // aktuelles Datum und Uhrzeit
  const formattedTime = `${('0' + date.getHours()).slice(-2)}:${('0' + date.getMinutes()).slice(-2)}`;
  updateClientsChart(formattedTime, data);
});

socket.on('zeit', function (msg) {
  let data = JSON.parse(msg.data);
  document.getElementById("timeContainer").innerHTML = String(data['hour']) + ":" + String(data['minute']) + ":" + String(data['second']);
});

socket.on('mqtt_messages', function (msg) {
  handleMQTTMsg(msg);
});