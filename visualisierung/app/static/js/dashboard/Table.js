var tblContent = [{
    Building: "Feuerwache",
    LastMQTTMessage: "",
    People: 0,
    EnergyBalance: 0
  },
  {
    Building: "Hochhaus",
    LastMQTTMessage: "",
    People: 0,
    EnergyBalance: 0
  },
  {
    Building: "Wohnhaus",
    LastMQTTMessage: "",
    People: 0,
    EnergyBalance: 0
  },
  {
    Building: "Tiefgarage",
    LastMQTTMessage: "",
    People: 0,
    EnergyBalance: 0
  },
  {
    Building: "Einkaufszentrum",
    LastMQTTMessage: "",
    People: 0,
    EnergyBalance: 0
  },
  {
    Building: "Katastrophenschutz",
    LastMQTTMessage: "",
    People: 0,
    EnergyBalance: 0
  },
  {
    Building: "Wetterstation",
    LastMQTTMessage: "",
    People: 0,
    EnergyBalance: 0
  },
  {
    Building: "Solarpark",
    LastMQTTMessage: "",
    People: 0,
    EnergyBalance: 0
  },
  {
    Building: "Flughafen",
    LastMQTTMessage: "",
    People: 0,
    EnergyBalance: 0
  },
  {
    Building: "Schule",
    LastMQTTMessage: "",
    People: 0,
    EnergyBalance: 0
  },
  {
    Building: "Polizeistation",
    LastMQTTMessage: "",
    People: 0,
    EnergyBalance: 0
  }
];

function updateTable(tableData) {
  // hole das HTML-Element für die Tabelle
  const tableContainer = document.getElementById("tblContainer");
  tableContainer.innerHTML = "";

  // Erstelle die Tabellenkopfzeile
  const tableHeader = document.createElement("thead");
  const headerRow = document.createElement("tr");

  // Füge die Spaltenüberschriften hinzu
  const headers = ["Building", "Last MQTT message", "People", "Energy balance"];
  headers.forEach(headerText => {
    const header = document.createElement("th");
    header.setAttribute("scope", "col");
    header.innerText = headerText;
    headerRow.appendChild(header);
  });

  // Füge die Kopfzeile zur Tabelle hinzu
  tableHeader.appendChild(headerRow);
  tableContainer.appendChild(tableHeader);

  // Erstelle die Tabellenkörperzeilen
  const tableBody = document.createElement("tbody");
  tableData.forEach(dataRow => {
    const row = document.createElement("tr");
    const buildingCell = document.createElement("th");
    buildingCell.setAttribute("scope", "row");
    buildingCell.innerText = dataRow.Building;
    row.appendChild(buildingCell);
    const mqttCell = document.createElement("td");
    mqttCell.innerText = dataRow.LastMQTTMessage;
    row.appendChild(mqttCell);
    const peopleCell = document.createElement("td");
    peopleCell.innerText = dataRow.People;
    row.appendChild(peopleCell);
    const energyCell = document.createElement("td");
    energyCell.innerText = dataRow.EnergyBalance;
    row.appendChild(energyCell);
    tableBody.appendChild(row);
  });

  // Füge den Tabellenkörper zur Tabelle hinzu
  tableContainer.appendChild(tableBody);
}

function handleMQTTMsg(msg) {
  let data = JSON.parse(msg.data);
  let topicLvl1 = String(msg.topic).toLowerCase().trim().split("/")[0];
  let topicLvl2 = "";
  try {
    topicLvl2 = String(msg.topic).toLowerCase().trim().split("/")[1];
  } catch(e) {
    topicLvl2 = ""
  }

  index = getIndexWhereTopicIsBuilding(topicLvl1);
  if (index == -1) {
    console.log("MQTT topic " + topicLvl1 + " could not be connected to a building.");
    return;
  }

  setMsgData(index, topicLvl2, data);

  updateTable(tblContent);
}

function getIndexWhereTopicIsBuilding(topicStr) {
  for (let i in tblContent) {
    if (String(tblContent[i]['Building']).toLowerCase().trim() === topicStr) {
      return i;
    }
  }
}

function setMsgData(index, topicLvl2, data) {
  if (index < 0 || topicLvl2 === "" | isNullOrEmpty(data)) {
    console.log("Not able to parse data.");
    return;
  }

  if (topicLvl2 === "energie") {
    let energy = "";
    if (isNullOrEmpty(data['available'])) {
      energy = String(data['allotted']);
    } else if (isNullOrEmpty(data['allotted'])) {
      energy = String(data['available']);
    } else if (!isNullOrEmpty(data['available']) && !isNullOrEmpty(data['allotted'])) {
      energy = String(data['available']) + " / " + String(data['allotted']);
    }
    tblContent[index]['EnergyBalance'] = energy;
  } else if (topicLvl2 === "menschen") {
    tblContent[index]['People'] = String(data['count']);
  }

  tblContent[index]['LastMQTTMessage'] = JSON.stringify(data);
}