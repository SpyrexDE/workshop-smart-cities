function showNotConnectedBanner() {
    document.getElementById("notConnectedBanner").style.height = "auto";
    document.getElementById("notConnectedBanner").style.visibility = "visible";
}

function hideNotConnectedBanner() {
    document.getElementById("notConnectedBanner").style.visibility = "hidden";
    document.getElementById("notConnectedBanner").style.height = "0px";
}

function disconnected() {
    // make connected badge invisible
    document.getElementById("conectedBadge").style.visibility = "hidden";
    document.getElementById("conectedBadge").style.width = "0px";

    // make disconnected badge visible
    document.getElementById("disconectedBadge").style.visibility = "visible";
    document.getElementById("disconectedBadge").style.width = "auto";
}

function connected() {
    // make disconnected badge invisible
    document.getElementById("disconectedBadge").style.visibility = "hidden";
    document.getElementById("disconectedBadge").style.width = "0px";

    // make connected badge visible
    document.getElementById("conectedBadge").style.visibility = "visible";
    document.getElementById("conectedBadge").style.width = "auto";
}