import logging

def setup_logger(name, log_file, level=logging.INFO):
    """To setup as many loggers as you want"""
    formatter = logging.Formatter('%(asctime)s | %(levelname)s \t| %(message)s')
    handler = logging.FileHandler(log_file, mode='w')
    handler.setFormatter(formatter)
    logger = logging.getLogger(name)
    logger.setLevel(level)
    logger.addHandler(handler)
    return logger

# Custom Logging for MQTT & Websockets connections
logger = setup_logger('logger', 'logging/logging.log')

# Logging der MQTT Messages
mqtt_logger = setup_logger('logging_mqtt', 'logging/mqtt.log')