# MINT EC Workshop 2023 | Visualiserung der Smart City
# Flask Applikation, die die Smart City monitoren kann


## Getting started

Zuerst sollten Sie dieses (Teil-)Projekt aus dem Ordner [`visualisierung/`](visualisierung/) herunterladen.

Außerdem ist es wichtig, dass Sie eine `venv` mit den in [`requirements.txt`](visualisierung/requirements.txt) definierten Paketen erstellen.

Dazu können Sie unter Windows verwenden: <br/>
`cd [Speicherort auf PC]/visualisierung` <br/>
`python -m venv venv/` <br/>
`.\venv\Scripts\activate` <br/>
`pip install -r requirements.txt`


Unter Linux können Sie verwenden: <br/>
`cd /pfad/zum/verzeichnis/visualisierung` <br/>
`python3 -m venv venv/` <br/>
`source venv/bin/activate` <br/>
`pip3 install -r requirements.txt`

## Config

Vor dem Start der Applikation müssen Sie eine Datei `config.py` erstellen, die folgenden Inhalt hat: <br/>
<code>#define flask variables
secret_key = "your secret key"
<br/>
#define MQTT variables
host = "your host"
port = 0 # your port
keep_alive = 60 # keep alive in seconds
user = "user"
pwd = "password"</code>

Sie wird im Ordner [`visualisierung/`](visualisierung/) abgespeichert.

## Deployment

Nun kann die Flask App gestartet werden.<br/>

Windows: <br/>
<code>set FLASK_APP=run
flask run</code>

Linux: <br/>
<code>export FLASK_APP=run.py
flask run</code>

Nun können Sie im Browser [http://127.0.0.1:5000](http://127.0.0.1:5000) aufrufen und die Webapplikation verwenden.

## Logs

Die Logs befinden sich in folgendem Ordner: [`logging/`](logging/).
* `falsk.log` enthält die Logs vom Flask Framework.
* `logging.log` enthält alle Logs, die mit der Verbindung zum MQTT Broker und zum Client per Websockets zu tun haben.
* `mqtt.log` enthält alle MQTT messages, die der Flask Server empfangen hat.
