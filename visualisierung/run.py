# Execute this file.

from app import app, socketio
from app.models.Logger import logger

if __name__ == '__main__':
    try:
        # try running Flask app with Websockets
        socketio.run(app, host="0.0.0.0", port=5000, debug=False, allow_unsafe_werkzeug=True)
    except Exception as e:
        logger.error("Some unknown error occured.", str(e))
        raise SystemExit