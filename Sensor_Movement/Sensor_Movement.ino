/*
 * Materialien:
 * 16in1_DE/HC-SR501 PIR Sensor DE.pdf
 * (ESP-32 Dev Kit C V2_DE.pdf)
 * 
 * Um den Output zu sehen
 * Tools >> Serial Monitor
 */

#define OUT_PIN 2

uint8_t output_value = 0;
bool motion_detected = false;

void setup() {
  Serial.begin(9600);
  pinMode(OUT_PIN, INPUT);
  delay(60000);
}

void loop() {
  output_value = digitalRead(OUT_PIN);
  if (output_value) {
    Serial.println("Object detected!");
    motion_detected = true;
    delay(3000);
  } else {
    Serial.println("No object!");
  }
  if (motion_detected) {
    Serial.println("Wait!");
    delay(6000);
    Serial.println("Ready...");
    motion_detected = false;
  }
}
