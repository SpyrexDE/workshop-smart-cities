/*
 * Materialien:
 * https://github.com/winlinvip/SimpleDHT
 * (16in1_DE/DHT11 Temperature Sensor Modul_DE.pdf)
 * (ESP-32 Dev Kit C V2_DE.pdf)
 * 
 * Um den Output zu sehen
 * Tools >> Serial Monitor
 */

#include <SimpleDHT.h>

int pinDHT11 = 2; //GPIO2
SimpleDHT11 dht11(pinDHT11);

void setup() { 
  Serial.begin(9600); //Serial Monitor
}
void loop() {
  
  Serial.println("Sample DHT11...");
  float temperature = 0;
  float humidity = 0;
  int err = SimpleDHTErrSuccess;
  
  if((err=dht11.read2(&temperature, &humidity, NULL)) != SimpleDHTErrSuccess){
    Serial.print("Read DHT11 failed, err=");
    Serial.println(err);
    
    delay(2000);
    return;
  }
  
  Serial.print("Sample OK: ");
  Serial.print((float)temperature);
  Serial.print(" *C, ");
  Serial.print((float)humidity);
  Serial.println(" RH%");
  
  delay(1500);
}
