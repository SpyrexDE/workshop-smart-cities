#include <WiFi.h>
#include <PubSubClient.h>
#include <ArduinoJson.h>


// Update these with values suitable for your network.
const char* ssid = "AP_NAME";
const char* password = "AP_PASS";
const char* mqtt_server = "87.106.170.226";
#define mqtt_port 1883
#define MQTT_USER "MQTT_USER"
#define MQTT_PASSWORD "MQTT_PASS"
#define MQTT_SERIAL_PUBLISH_TOPIC "PUBLISH_TOPIC"
#define MQTT_SERIAL_RECEIVER_TOPIC "RECEIVE_TOPIC"

const int BUFFER_SIZE = JSON_OBJECT_SIZE(300);

WiFiClient wifiClient;
PubSubClient client(wifiClient);

void setup_wifi() {
  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }
  randomSeed(micros());
  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP32Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str(), MQTT_USER, MQTT_PASSWORD)) {
      Serial.println("connected");
      //Once connected, publish an announcement...
      client.publish("/test", "hello world");
      // ... and resubscribe
      client.subscribe(MQTT_SERIAL_RECEIVER_TOPIC);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void callback(char* topic, byte *payload, unsigned int length) {
  
  StaticJsonDocument<BUFFER_SIZE> jsonDoc;
  deserializeJson(jsonDoc, payload, length);

  if(!jsonDoc["string"].isNull()) {
    String printable = jsonDoc["string"].as<String>();
    Serial.println(printable);
  }

  if(!jsonDoc["int"].isNull()) {
    int number = jsonDoc["int"].as<int>();
    Serial.println(number);
  }

  JsonArray jsonArray = jsonDoc["array"];
  if (!jsonArray.isNull()) {
    for(JsonVariant v : jsonArray) {
      int number = v.as<int>();
      Serial.println(number);
    }
  }

  JsonObject jsonObj;
  jsonObj = jsonDoc.as<JsonObject>();
  jsonObj = jsonDoc["object"];

  /*
   * Sobald eine Nachricht gesendet wird, leert sich das JsonDoc automatisch. 
   * Deshalb erst alles empfangen und dann senden!
   */
}

void setup() {
  Serial.begin(115200);
  Serial.setTimeout(500);// Set time out
  setup_wifi();
  client.setBufferSize(BUFFER_SIZE);
  client.setServer(mqtt_server, mqtt_port);
  client.setCallback(callback);
  reconnect();
}

void publishStringData(char *stringData){
  if (!client.connected()) {
    reconnect();
  }
  client.publish(MQTT_SERIAL_PUBLISH_TOPIC, stringData);
}

void publishJsonData(StaticJsonDocument<BUFFER_SIZE> jsonData){
  if (!client.connected()) {
    reconnect();
  }

  char buffer[measureJson(jsonData) + 1];
  serializeJson(jsonData, buffer, sizeof(buffer));
  client.publish(MQTT_SERIAL_PUBLISH_TOPIC, buffer, true);
}

void loop() {
   client.loop();
   if (!true) {
     publishStringData("{'test': 2}");
   }
 }
