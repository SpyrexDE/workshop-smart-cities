# Workshop Smart Cities

Ein Workshop, in dem mithilfe von ESP32 Mikrocomputern eine Smarte City mit mehreren Gebäuden und Sensoren aufgebaut werden soll. Ziel ist es, dass alle Gebäude smart über MQTT miteinander kommunizieren.