/*
 * Materialien:
 * 16in1_DE/SW420 Vibration Sensor Module DE.pdf
 * (ESP-32 Dev Kit C V2_DE.pdf)
 * 
 * Um den Output zu sehen
 * Tools >> Serial Monitor
 */

#define DIGITAL_PIN 2

boolean vibrating = false;
String vibration;

void setup() {
  Serial.begin(9600);
  pinMode(DIGITAL_PIN, INPUT);
}

void loop() {
  vibrating = digitalRead(DIGITAL_PIN);
  if (vibrating) {
    vibration = "Yes";
  }
  else {
    vibration = "No";
  }

  Serial.print("Vibration detected: ");
  Serial.println(vibration);
  delay(10);
}
