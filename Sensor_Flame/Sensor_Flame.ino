/*
 * Materialien:
 * 16in1_DE/Flame sensor module_DE.pdf
 * (ESP-32 Dev Kit C V2_DE.pdf)
 * 
 * Um den Output zu sehen
 * Tools >> Serial Monitor
 */


#define DIGITAL_PIN 2

boolean flame = false;

void setup() {
  Serial.begin(9600);
  pinMode(DIGITAL_PIN, INPUT);
}

void loop() {
  flame = digitalRead(DIGITAL_PIN);
  
  if (!flame) {
    Serial.println("Flame detected!");
  }

  delay(1000);
}
