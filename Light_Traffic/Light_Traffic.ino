/*
 * Materialien:
 * 16in1_DE/LED Traffic Light Module_DE.pdf
 * (ESP-32 Dev Kit C V2_DE.pdf)
 */

int GREEN = 16;
int YELLOW = 4;
int RED = 2;

void setup()
{
  pinMode(GREEN, OUTPUT);
  pinMode(YELLOW, OUTPUT);
  pinMode(RED, OUTPUT);
}

void loop()
{
  digitalWrite(GREEN, LOW);
  digitalWrite(YELLOW, LOW);
  digitalWrite(RED, HIGH);
  delay(3000);
  
  digitalWrite(YELLOW, HIGH);
  delay(1000);
  
  digitalWrite(RED, LOW);
  digitalWrite(YELLOW, LOW);
  digitalWrite(GREEN, HIGH);
  delay(3000);
  
  digitalWrite(GREEN, LOW);
  digitalWrite(YELLOW, HIGH);
  delay(1000);
}
