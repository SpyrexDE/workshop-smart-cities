# Dieses Skript erzeugt alle drei Sekunden eine random Nachricht auf ein random Topic und sendet dies per MQTT.
# Es dient als Test für die MQTT Konsole dieses Softwareprojekts zur Visualierung der Smart City.

import paho.mqtt.client as mqtt
import time
import random
import string
import config

def on_connect(client, userdata, flags, rc):
    print(rc)

try:
    # try connecting with MQTT broker
    client = mqtt.Client()
    client.on_connect = on_connect
    client.username_pw_set(username=config.user, password=config.pwd)
    client.connect(config.host, config.port, config.keep_alive)
    client.subscribe("vorstellungsrunde")
    client.loop_start()
except Exception as e:
    print(str(e))
    raise SystemExit

alphabet = string.ascii_uppercase

# Zufällige Nachrichten senden
while True:
    if random.randint(0, 1) == 1:
        topic_lvl1 = str(random.choice(alphabet)) + "/"
    else:
        topic_lvl1 = ""
    topic = topic_lvl1 + str(random.choice(alphabet))
    message = "Content " + str(random.randint(1, 100)) + str(random.choice(alphabet))
    print("Topic: " + str(topic) + "\nMessage: " + str(message))
    client.publish(topic, message)
    time.sleep(3)