/*
 * Materialien:
 * 16in1_DE/HC-SR04 Ultrasonic Sensor Module DE.pdf
 * (ESP-32 Dev Kit C V2_DE.pdf)
 * 
 * Um den Output zu sehen
 * Tools >> Serial Monitor
 */

#define TRIG_PIN 4 //GPIO4
#define ECHO_PIN 2 //GPIO2

long duration, cm, inches;

void setup() {
  Serial.begin(9600); //Serial Monitor
  pinMode(TRIG_PIN, OUTPUT);
  pinMode(ECHO_PIN, INPUT);
}

void loop() {
  digitalWrite(TRIG_PIN, LOW);
  delayMicroseconds(5);
  digitalWrite(TRIG_PIN, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIG_PIN, LOW);
  
  duration = pulseIn(ECHO_PIN, HIGH);
  cm = (duration / 2) / 29.1; // Divide by 29.1 or multiply by 0.0343
  inches = (duration / 2) / 74; // Divide by 74 or multiply by 0.0135
  
  Serial.print(inches);
  Serial.print("in, ");
  Serial.print(cm);
  Serial.print("cm");
  Serial.println();
  
  delay(500);
}
